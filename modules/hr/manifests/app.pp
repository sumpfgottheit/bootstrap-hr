class hr::app (
    $apphome='/opt/hr/application', 
    $virtualenv='/opt/hr/virtualenv', 
    $user='hr'
) {

    vcsrepo { "$apphome":
        ensure   => latest,
        revision => 'ace131',
        provider => git,
        source   => 'git@bitbucket.org:sumpfgottheit/hrng.git',
        user     => $user,
        require  => [Class['hr::knownhosts'], User[$user]]
    }

    File {
        owner => $user,
        group => $user
    }

    file { ["$apphome/logs", "$apphome/run"]:
        ensure  => directory,
        require => Vcsrepo["$apphome"]
    }
    
#        client_package_name  => 'postgresql93',
#        server_package_name  => 'postgresql93-server',
#        contrib_package_name => 'postgresql93-contrib',
#        devel_package_name   => 'postgresql93-devel',
#        service_name         => 'postgresql-9.3'
    class { 'postgresql::globals':
        encoding            => 'UTF8',
        version             => '9.3',
        manage_package_repo => 'true'
    }
    class { 'postgresql::lib::devel': }
    class { 'postgresql::server': 
            listen_addresses  => 'localhost',
            postgres_password => 'oracle!',
            require           => Class['postgresql::globals']
    }
    postgresql::server::db { 'hrng':
        user     => 'uHrng',
        password => postgresql_password('uHrng', 'oracle'),
    }
    
}
