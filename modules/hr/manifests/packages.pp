class hr::packages {

    class {'repo_centos': 
        enable_scl => true
    }

    class { 'rpmforge':
        enabled => 0
    }

    yumrepo { 'ius':
        name     => 'ius',
        baseurl  => 'http://dl.iuscommunity.org/pub/ius/stable/CentOS/6/x86_64/',
        enabled  => 0,
        gpgcheck => 0,
    }

    $latest_packages = [ 'python27', 'python27-python-devel', 'vim-enhanced', 'python-setuptools', 'python-pip', 'libxml2-devel', 'libxslt-devel', 'gcc', 'czmq', 'czmq-devel']
    package { $latest_packages:
        ensure  => latest,
        require => [Class['repo_centos', 'rpmforge'], Yumrepo['ius']]
    }

}
