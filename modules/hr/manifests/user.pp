class hr::user($home='/opt/hr', $homemode='0700') {

    $user = 'hr'
    $_sshdir = "${home}/.ssh"

    File {
        owner => $user,
        group => $user,
    }


    user { $user:
        groups     => ['wheel'],
        managehome => 'false',
        uid        => '801',
        home       => $home
    }

    file { $home:
        ensure  => 'directory',
        mode    => $homemode,
        require => User[$user]
    }


    file { "${_sshdir}":
        ensure  => directory,
        mode    => '0700',
        require => File[$home]
    }

    file { "${_sshdir}/id_rsa":
        ensure  => present,
        source  => 'puppet:///modules/hr/hr_ssh_dir/id_rsa',    # Bitbucket Deployment Key Private Part
        mode    => '0600',
        require => File["${home}/.ssh"]
    }

    file { "${_sshdir}/id_rsa.pub":
        ensure  => present,
        source  => 'puppet:///modules/hr/hr_ssh_dir/id_rsa.pub',    # Bitbucket Deployment Key Public Part
        mode    => '0644',
        require => File["${home}/.ssh"]
    }

    file  { "${_sshdir}/authorized_keys":
        ensure  => present,
        mode    => '0600',
        require => File["${_sshdir}"]
    }

    file_line { 'ssh-key-saf':
        ensure  => present,
        line    => 'ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAl7iIrF6UktHrBctk72uKksDt+1bM6l6xckVAa55V0MRAtag2GJ3DCbeugbXTDjKogOD5yaG671loZ96KmHNqCNe64fOfhKFbMr1OlXc5i7ylVbqEZCYVKKRvBHtyJq327SLDMQAnqsp1/u34/XzUNSkniscHTSXru2vZmllDsjVYr/CrWJxNl4po/3u7nwqNPW0AtCbY5dFARm31ePNxYQt4tcOLTTgs43aV2Y32kUDQevFro1d90cKOCbMqLFWpOQt13gPWmdJi6LX7ffKdTXCn303zp/KWUBbGZVZgMkEkqL/IEAfaMcy9xEXdRm22hXdKINZ9HgYLesFTeAPcQQ== saf@Powerbook.local',
        path    => "${_sshdir}/authorized_keys",
        require => File["${_sshdir}/authorized_keys"]
    }

    vcsrepo { "${home}/dotfiles":
        ensure   => latest,
        provider => git,
        source   => 'https://github.com/sumpfgottheit/dotfiles.git',
        user     => $user,
        revision => 'master',
        require  => [User[$user], Class['git'], File[$home]]
    }

    exec { "run-env-make-${user}":
        path      => '/sbin:/bin:/usr/sbin:/usr/bin',
        cwd       => "${home}/dotfiles",
        command   => 'make',
        subscribe => Vcsrepo["${home}/dotfiles"],
        refreshonly => true
    }


}
