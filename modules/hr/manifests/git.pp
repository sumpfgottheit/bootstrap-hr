class hr::git {
	Exec {
		path => '/bin:/sbin:/usr/bin:/usr/sbin'
	}

    package { 'perl-TermReadKey':
        ensure  => 'latest',
        require => [Class['repo_centos', 'epel'], Yumrepo['ius']]
    }

	exec { 'uninstall-git17':
		command => "yum -y remove git perl-Git",
		unless  => "rpm -q git18",
        require => Package['perl-TermReadKey']
	}

	exec { 'install-git18':
		command => 'yum -y --disablerepo="*" --enablerepo="ius" install git18',
		subscribe => Exec['uninstall-git17']
	}
}
