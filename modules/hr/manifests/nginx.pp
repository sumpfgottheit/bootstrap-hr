class hr::nginx($ssl=true) {

    case $fqdn {
        'lnurn3.schlof.net': {
            $server_name = 'hr2.schlof.net'
            $nginx_conf = 'hr/hrng.conf.ssl.erb'
            $use_ssl = true
            $manage_firewall = false
        }
        'hrng.cloudapp.net': {
            $server_name = 'hr.schlof.net'
            $nginx_conf = 'hr/hrng.conf.ssl.erb'
            $use_ssl = true
            $manage_firewall = true
        }
        default: {
            $server_name = $::fqdn
            $nginx_conf = 'hr/hrng.conf.nossl.erb'
            $use_ssl = false
            $manage_firewall = true
        }
    }

    if $manage_firewall {
        firewall { '100 allow http access':
            port   => 80,
            proto  => tcp,
            action => accept,
        }
        if $use_ssl {
            firewall { '101 allow https access':
                port   => 443,
                proto  => tcp,
                action => accept,
            }
        }
    }

    yumrepo { 'nginx':
        name      => 'nginx',
        baseurl   => 'http://nginx.org/packages/centos/$releasever/$basearch/',
        gpgcheck => 0,
        enabled   => 1
    }

    package { 'nginx':
        ensure  => 'latest',
        require => Yumrepo['nginx']
    }

    file { '/etc/nginx/conf.d/hrng.conf':
        content => template($nginx_conf),
        notify  => Service['nginx']
    }

    service { 'nginx':
        ensure  => 'running',
        enable  => 'true',
        require => Package['nginx']
    }
}
