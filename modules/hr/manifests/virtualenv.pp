class hr::virtualenv(
    $virtualenv_path='/opt/hr/virtualenv', 
    $app_path='/opt/hr/application', 
    $requirements='/opt/hr/application/requirements.txt',
    $system_site_packages=true,
    $user = 'hr'
) {

    exec { 'create_venv':
        path      => '/bin:/usr/bin:/usr/local/bin:',
        command   => "bash -c 'source /opt/rh/python27/enable && virtualenv --system-site-packages ${virtualenv_path}'",
        logoutput => on_failure,
        creates   => $virtualenv_path,
        user      => $user,
    }

    file_line { 'virtualenv-pythonpath':
        ensure    => present,
        line      => "export PYTHONPATH=$app_path:\$PYTHONPATH",
        path      => "$virtualenv_path/bin/activate",
        subscribe => Exec['create_venv']
    }

    file_line { 'virtualenv-path':
        ensure    => present,
        line      => "export PATH=${app_path}/bin:/usr/pgsql-9.3/bin:\$PATH",
        path      => "$virtualenv_path/bin/activate",
        subscribe => File_line['virtualenv-pythonpath']
    }

    exec { 'source_scl':
        path      => '/bin:/usr/bin',
        command   => 'grep -F \'[[ $X_SCLS =~ "python27" ]] || (. /opt/rh/python27/enable && export X_SCLS="$X_SCLS python27")\' /opt/hr/virtualenv/bin/activate || sed -i \'1i[[ $X_SCLS =~ "python27" ]] || (. /opt/rh/python27/enable && export X_SCLS="$X_SCLS python27")\' /opt/hr/virtualenv/bin/activate ',
        subscribe => File_line['virtualenv-path']
    }

    exec { 'install_from_requirements':
        path      => '/bin:/usr/bin:/usr/local/bin:',
        command   => "bash -c 'source /opt/rh/python27/enable && source /opt/hr/virtualenv/bin/activate && pip install -r ${requirements}'",
        logoutput => on_failure,
        user      => $user,
        require   => Exec['source_scl']
    }
        
}
