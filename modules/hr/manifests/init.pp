class hr {

    File {
        owner => 'root',
        group => 'root'
    }

    $home = "/opt/hr"
    $apphome = "${home}/application"
    $virtualenv = "${home}/virtualenv"

    class { 'hr::user': 
        home     => $home ,
        homemode => '0755'
    }
    include hr::knownhosts
    include hr::packages
    include hr::git
    include hr::supervisord
    include hr::redis
    include epel
    include hr::nginx

    class { 'hr::app': 
        apphome    => $apphome,
        virtualenv => $virtualenv,
        user       => $user,
        require    => Class['hr::git']
    }
    
    class { 'hr::virtualenv':
        require => Class['hr::app']
    }


    file { '/usr/local/bin/puppetrun':
        source => 'puppet:///modules/hr/puppetrun',
        mode   => '0755'
    }

    file { '/etc/sudoers.d/wheel':
        content => "%wheel ALL=(ALL) NOPASSWD: ALL\n",
        mode    => '0440'
    }


}
