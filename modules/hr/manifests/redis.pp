class hr::redis {

    yumrepo { 'remi':
        name     => 'remi',
        baseurl  => 'http://rpms.famillecollet.com/enterprise/6/remi/$basearch/',
        enabled  => 0,
        gpgcheck => 0,
        require  => Class['repo_centos', 'epel']
    }

    Exec {
        path => '/bin:/sbin:/usr/bin:/usr/sbin'
    }

    exec { 'install-redis':
        command => 'yum -y --enablerepo="remi" install redis',
        unless  => 'rpm -q redis',
        require => Yumrepo['remi']
    }

    service { 'redis':
        ensure  => 'running',
        enable  => 'true',
        require => Exec['install-redis']
    }
}
