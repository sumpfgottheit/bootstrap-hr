class hr::supervisord {
    
    File {
        owner => 'root',
        group => 'root'
    }

    python::pip { 'supervisor':
        ensure   => 'present',
        require => Package['python-pip']
    }
    file { '/etc/init.d/supervisord':
        source  => 'puppet:///modules/hr/supervisord.init',
        mode    => '0755',
        require => Python::Pip['supervisor']
    }
    file { 'supervisord.conf':
        source  => 'puppet:///modules/hr/supervisord.conf',
        path    => '/etc/supervisord.conf',
        mode    => '0664',
        require => Python::Pip['supervisor']
    }

    service { 'supervisord':
        ensure  => 'running',
        enable  => 'true',
        require => [File['/etc/init.d/supervisord'], File['supervisord.conf'], File['/etc/supervisord.d/hr.conf']]
    }

    file { '/etc/supervisord.d':
        ensure => 'directory',
    }

    file { '/etc/supervisord.d/hr.conf':
        source  => 'puppet:///modules/hr/hr.supervisord',
        mode    => '0664',
        require => File['/usr/local/bin/runin_scl_env.sh'],
        notify  => Service['supervisord']
    }

    file { '/etc/supervisord.d/rqworker.conf':
        source  => 'puppet:///modules/hr/rqworker.supervisord',
        mode    => '0664',
        require => File['/usr/local/bin/runin_scl_env.sh'],
        notify  => Service['supervisord']
    }

    file {'/usr/local/bin/runin_scl_env.sh':
        source  => 'puppet:///modules/hr/runin_scl_env.sh',
        mode    => '0755'
    }

    file {'/usr/local/bin/hr':
        source  => 'puppet:///modules/hr/hr',
        mode    => '0755'
    }

    file { '/usr/local/bin/spc':
        ensure => 'link',
        target => '/usr/bin/supervisorctl'
    }

    file { '/etc/sudoers.d/hr':
        source => 'puppet:///modules/hr/hr.sudoers',
        mode   => 0440,
        owner  => 'root',
        group  => 'root'
    }

}
