#!/bin/bash
SCL=$1
VENV=$2
if [ -z $SCL ]; then
    echo "usage: runin_scl_env [scl] [virtualenv_path] CMDS"
    exit 1
fi

if [ -z $VENV ]; then
    echo "usage: runin_scl_env [scl] [virtualenv_path] CMDS"
    exit 1
fi

. /opt/rh/${SCL}/enable
export X_SCLS="$X_SCLS python27 "
shift 1
. ${VENV}/bin/activate
shift 1
echo "Executing $@ in ${VENV}"
exec "$@"
deactivate
