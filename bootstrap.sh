#!/bin/bash

rpm -q centos-release-SCL || yum -y install centos-release-SCL
rpm -q ruby193 || yum -y install ruby193 ruby193-ruby-devel
rpm -q libxslt-devel || yum -y install libxslt-devel

#
# Install Puppet and Git
#
if ! rpm -q puppet ; then
	echo "=== Install puppet yum repository ==="
	sudo rpm -Uvh https://yum.puppetlabs.com/el/6/products/x86_64/puppetlabs-release-6-10.noarch.rpm
	echo "=== Install puppet and git ==="
	sudo yum -y install centos-release-SCL
	sudo yum -y install ruby193 ruby193-ruby-devel
	sudo yum -y install puppet git 
fi
which git
[[ $? == 0 ]] || sudo yum -y install git

#
# Add Hostkey from Github and Bitbucket to global known hosts
#
host_key_bitbucket='bitbucket.org,131.103.20.167 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw=='
host_key_github='github.com,192.30.252.129 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ=='
grep -q bitbucket.org /etc/ssh/ssh_known_hosts
[[ $? == 0 ]] || echo $host_key_bitbucket >> /etc/ssh/ssh_known_hosts
grep -q github.com /etc/ssh/ssh_known_hosts
[[ $? == 0 ]] || echo $host_key_github >> /etc/ssh/ssh_known_hosts

#
# Get the bootstrap-hr repository (Public Repository, so no Deployment Key is necessary)
#
echo "=== Fetch bootstrap-hr-puppet git repository ==="
cd /opt
if [[ ! -d bootstrap-hr ]] ; then
	git clone https://bitbucket.org/sumpfgottheit/bootstrap-hr.git
else 
	cd bootstrap-hr
	git pull --rebase 
	cd ..
fi

if ! grep '/opt/rh/ruby193/root/usr/local/bin/' /opt/rh/ruby193/enable ; then
	echo export PATH=/opt/rh/ruby193/root/usr/local/bin/:\$PATH >> /opt/rh/ruby193/enable
fi
. /opt/rh/ruby193/enable

cd /opt/bootstrap-hr/modules

#
# Run Puppet
#
LIBRARIAN=$(which librarian-puppet)
if [[ ! -x $LIBRARIAN ]] ; then
	echo "Installing Librarian Puppet - will take about 2 minutes"
	gem install librarian-puppet --no-ri --no-rdoc --verbose
fi
librarian-puppet install
echo "Outdated Modules"
librarian-puppet outdated
echo "Call Puppet"
mv /etc/yum.repos.d /etc/yum.repos.d.old
mkdir /etc/yum.repos.d
echo puppet apply --modulepath=/opt/bootstrap-hr/modules:/opt/bootstrap-hr/modules/modules --verbose /opt/bootstrap-hr/manifests/site.pp > /usr/local/bin/puppet-apply
chmod 0755 /usr/local/bin/puppet-apply
echo puppet apply --modulepath=/opt/bootstrap-hr/modules:/opt/bootstrap-hr/modules/modules --verbose /opt/bootstrap-hr/manifests/site.pp
echo 
echo "/usr/local/bin/puppet-apply"
echo
